package br.com.loteria;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONException;

public class JSONRead {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//http://wsloterias.azurewebsites.net/gettingstarted
		//http://wsloterias.azurewebsites.net/api/sorteio/getresultado/1/1763

		try {

			org.json.JSONObject json = readJsonFromUrl(
					"http://wsloterias.azurewebsites.net/api/sorteio/getresultado/1/1763");
			System.out.println(json.toString());
			System.out.println("Valor");

			JSONArray results = json.getJSONArray("Sorteios");

			System.out.println("Numeros do Soteio: " + json.get("NumeroConcurso"));
			System.out.println(results.get(0));
			
			org.json.JSONObject teste = new JSONObject(results.get(0).toString());
			
			JSONArray results2 = teste.getJSONArray("Numeros");
			
			for (int i = 0; i < results2.length(); i++) {
				System.out.println(results2.get(i));
			}
			

			
		}
		// Trata as exceptions que podem ser lançadas no decorrer do processo
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
			// } catch (ParseException e) { // TODO Auto-generated catch block
			// e.printStackTrace();
		}

	}

	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

	public static org.json.JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
		InputStream is = new URL(url).openStream();
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			org.json.JSONObject json = new org.json.JSONObject(jsonText);
			return json;
		} finally {
			is.close();
		}
	}

}
